# nixos


```
mkdir -p tmp
wget https://channels.nixos.org/nixos-23.05/latest-nixos-minimal-x86_64-linux.iso -O tmp/nixos.iso
qemu-img create -f raw tmp/nixos.img 20G
qemu-system-x86_64 -enable-kvm -cpu 4 -m 8G -nic user,hostfwd=tcp::8888-:22 -cdrom nixos.iso -boot d
# -> passwd
ssh -o StrictHostKeyChecking=no -o PreferredAuthentications=password nixos@127.0.0.1 -p 8888
```


<!-- garbi- collector -->

<!-- nixos-rebuild build-vm -> wie groß ist die vm und wi egreife ich darauf zu?y -->

<!---
-cpu <CPU> - Specify a processor architecture to emulate. To see a list of supported architectures, run: qemu-system-x86_64 -cpu ?
-cpu host - (Recommended) Emulate the host processor.
-smp <NUMBER> - Specify the number of cores the guest is permitted to use. The number can be higher than the available cores on the host system. Use -smp $(nproc) to use all currently available cores.
--->


- Gitrepo downloaden: git clone https://gitlab.com/cmerbach/nixos.git && cd nixos
- chmod +x run.sh
- ausführen von: ./run.sh
- You mustr set a password at the end of the script for root
- after areeboot please chnage the user password
-> sudo reboot/poweroff
- passwort setzen -> root password
-> Hochfahren und dann user pasword ändern -> sudo passwd user default: Start123!
nixos-rebuild switch

<!--
Erstellen eines raid system
# https://www.linuxbabe.com/linux-server/linux-software-raid-1-setup
# https://wpguru.co.uk/2021/01/expand-software-raid-mdadm/
gegebenfalls raid-configuration.nix anpassen
-->

<!--
## Short overview/excursion into using ```git-crypt```

To ensure that the credentials and tokens are securely stored, they are encrypted in the repo directly. The secrets are also available as environment variables in the repo, but it is necessary if the repo is run locally that the secrets/token are in the [.secrets](./secrets/) folder. Also the file for setting global variables is in this folder because it contains sensitive user information and workspace information.

The following commands are required to create a key and assign it to a repo:

```bash
# create a key e.g.: admin.automation@exb.de
gpg --generate-key
# export a key
gpg --output ../public.gpg --armor --export admin.automation@exb.de
gpg --output ../privat.gpg --armor --export-secret-key admin.automation@exb.de
# edit/trust a key
gpg --edit-key admin.automation@exb.de
-> trust -> 5 -> j -> q

# init git-crypt for the repo
git crypt init
# add the key to the reo
git crypt add-gpg-user admin.automation@exb.de
# creat the .gitattributes file with the following content
echo ".secrets/** filter=git-crypt diff=git-crypt" > .gitattributes

# informational/additional - How to delete a key
# gpg --delete-secret-key [uid]
# gpg --delete-key
```

Before a change or lock by git-crypt, all files must be staged. Then you can use: ```git crypt lock``` or ```git crypt unlock```. By using .gitattributes, the encryption happens automatically when you push into the repo.

If you don't have the file needed for the repo yet, you can find it in **1password**. You can import this to your local computer with the command ```gpg --import privat.gpg``` Remember that you must run the edit command for this key in order to trust this key.
-->


<!--
nix-env -iA nixos.git

-> sudo sed -i '$i\'"environment.systemPackages = with pkgs; [git];" "/etc/nixos/configuration.nix"
-> sudo nixos-rebuild switch


- Sudo Authentication with Yubikey
    sudo apt-get install libpam-u2f pamu2fcfg
    sudo nano /etc/udev/rules.d/70-u2f.rules
        KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0664", GROUP="plugdev"
sudo pamu2fcfg -u user` -opam://`hostname` -ipam://`hostname`
sudo nano /etc/u2f_mappings
https://www.youtube.com/watch?v=_p6NemXF_BY
sudo nano /etc/pam.d/sudo
-->